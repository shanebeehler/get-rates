import React from 'react';
import {shallow} from './enzyme';
import {App} from './App';
import Loading from './components/Loading';
import RatesTable from './components/RatesTable';

describe('App tests', () => {
  it('should call get rates when button is clicked', () => {
    const getRates = jest.fn();
    const wrapper = shallow(<App getRates={getRates} />);
    wrapper.find('button').simulate('click');
    expect(getRates).toHaveBeenCalled();
  });

  it('renders Loading component when requestingGetRates is true', () => {
    const wrapper = shallow(<App requestingGetRates={true} />);
    expect(wrapper.find(Loading)).toHaveLength(1);
  });

  it('renders base when ratesData is truthy', () => {
    const ratesData = {
      base: 'EUR',
      rates: {BGN: 1.9558, NZD: 1.7259},
      date: '2019-06-17'
    };
    const wrapper = shallow(<App ratesData={ratesData} />);
    expect(wrapper.contains(ratesData.base)).toBeTruthy();
  });

  it('renders date when ratesData is truthy', () => {
    const ratesData = {
      base: 'EUR',
      rates: {BGN: 1.9558, NZD: 1.7259},
      date: '2019-06-17'
    };
    const wrapper = shallow(<App ratesData={ratesData} />);
    expect(wrapper.contains(ratesData.date)).toBeTruthy();
  });

  it('renders RatesTable when ratesData is truthy', () => {
    const ratesData = {
      base: 'EUR',
      rates: {BGN: 1.9558, NZD: 1.7259},
      date: '2019-06-17'
    };
    const wrapper = shallow(<App ratesData={ratesData} />);
    expect(wrapper.find(RatesTable)).toHaveLength(1);
  });
});
