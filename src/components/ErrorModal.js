import React from 'react';
import Modal from 'react-modal';

const ErrorModal = ({errorModalIsVisible, closeErrorModal, errorMessage}) => {
  return (
    <Modal isOpen={errorModalIsVisible} contentLabel="Error Modal">
      <h4>{errorMessage}</h4>
      <button onClick={closeErrorModal}>Okay</button>
    </Modal>
  );
};

export default ErrorModal;
