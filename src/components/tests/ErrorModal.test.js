import React from 'react';
import {findDOMNode} from 'react-dom';
import {shallow} from '../../enzyme';
import Modal from 'react-modal';
import ErrorModal from '../ErrorModal';

describe('ErrorModal tests', () => {
  it('Renders error message', () => {
    const errorMessage = 'Error message';
    const wrapper = shallow(<ErrorModal errorMessage={errorMessage} />);
    expect(wrapper.find('h4').text()).toEqual(errorMessage);
  });

  it('modal is open when errorModalIsVisible is true', () => {
    const wrapper = shallow(<ErrorModal errorModalIsVisible={true} />);
    expect(wrapper.find(Modal).prop('isOpen')).toEqual(true);
  });

  it('should call closeErrorModal when button is clicked', () => {
    const closeErrorModal = jest.fn();
    const wrapper = shallow(
      <ErrorModal
        closeErrorModal={closeErrorModal}
        errorModalIsVisible={true}
      />
    );
    wrapper.find('button').simulate('click');
    expect(closeErrorModal).toHaveBeenCalled();
  });
});
