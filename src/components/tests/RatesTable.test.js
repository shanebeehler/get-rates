import React from 'react';
import {shallow} from '../../enzyme';
import RatesTable from '../RatesTable';

const rates = {BGN: 1.9558, NZD: 1.7259};
const wrapper = shallow(<RatesTable rates={rates} />);

describe('RatesTable tests', () => {
  it('renders right amount of td elements', () => {
    expect(wrapper.find('td')).toHaveLength(4);
  });

  it('renders a tr with td data', () => {
    expect(
      wrapper.contains(
        <tr key="BGN">
          <td>BGN</td>
          <td>{1.9558}</td>
        </tr>
      )
    ).toBeTruthy();
  });
});
