import React from 'react';

const RatesTable = ({rates}) => (
  <table>
    <tbody>
      <tr>
        <th>Currency</th>
        <th>Exchange Rate</th>
      </tr>
      {Object.keys(rates).map(currencyKey => (
        <tr key={currencyKey}>
          <td>{currencyKey}</td>
          <td>{rates[currencyKey]}</td>
        </tr>
      ))}
    </tbody>
  </table>
);

export default RatesTable;
