import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import * as actions from './redux/appActions';
import Loading from './components/Loading';
import RatesTable from './components/RatesTable';
import ErrorModal from './components/ErrorModal';

export let App = ({
  getRates,
  requestingGetRates,
  ratesData,
  clearRatesError,
  errorLoadingRates
}) => {
  return (
    <div>
      <button onClick={getRates}>Get Exchange Rates</button>
      {requestingGetRates && <Loading />}
      {ratesData && (
        <Fragment>
          <div>Base: {ratesData.base}</div>
          <div>Date: {ratesData.date}</div>
          <RatesTable rates={ratesData.rates} />
        </Fragment>
      )}
      <ErrorModal
        errorModalIsVisible={errorLoadingRates}
        errorMessage="An error occured loading rates."
        closeErrorModal={clearRatesError}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  requestingGetRates: state.requestingGetRates,
  ratesData: state.ratesData,
  errorLoadingRates: state.errorLoadingRates
});

export default connect(
  mapStateToProps,
  actions
)(App);
