import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moxios from 'moxios';
import {
  GET_RATES_REQUEST,
  GET_RATES_SUCCESS,
  GET_RATES_FAILURE
} from '../appTypes';
import * as actions from '../appActions';
import successfulResponseData from './successfulResponseData';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('getPosts actions', () => {
  beforeEach(function() {
    moxios.install();
  });

  afterEach(function() {
    moxios.uninstall();
  });

  it('creates GET_RATES_SUCCESS after successfuly fetching rates', () => {
    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: successfulResponseData
      });
    });

    const expectedActions = [
      {type: GET_RATES_REQUEST},
      {type: GET_RATES_SUCCESS, payload: successfulResponseData}
    ];

    const store = mockStore({ratesData: successfulResponseData});

    return store.dispatch(actions.getRates()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('creates GET_RATES_FAILURE after error fetching rates', () => {
    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 400,
        response: successfulResponseData
      });
    });

    const expectedActions = [
      {type: GET_RATES_REQUEST},
      {type: GET_RATES_FAILURE}
    ];

    const store = mockStore({ratesData: null});

    return store.dispatch(actions.getRates()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
