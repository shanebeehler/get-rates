import {createStore, applyMiddleware} from 'redux';
import reduxThunk from 'redux-thunk';
import appReducer from './appReducer';
import {composeWithDevTools} from 'redux-devtools-extension';

const createStoreWithMiddleware = composeWithDevTools(
  applyMiddleware(reduxThunk)
)(createStore);

export default createStoreWithMiddleware(appReducer);
