import axios from 'axios';
import {
  GET_RATES_REQUEST,
  GET_RATES_SUCCESS,
  GET_RATES_FAILURE,
  CLEAR_RATES_ERROR
} from './appTypes';

export function getRates() {
  return function(dispatch) {
    dispatch({type: GET_RATES_REQUEST});
    return axios
      .get(`https://api.exchangeratesapi.io/latest`)
      .then(response => {
        dispatch({
          type: GET_RATES_SUCCESS,
          payload: response.data
        });
      })
      .catch(error => {
        dispatch({
          type: GET_RATES_FAILURE
        });
      });
  };
}

export function clearRatesError() {
  return {
    type: CLEAR_RATES_ERROR
  };
}
