import {
  GET_RATES_REQUEST,
  GET_RATES_SUCCESS,
  GET_RATES_FAILURE,
  CLEAR_RATES_ERROR
} from './appTypes';

export default function(state = {}, action) {
  switch (action.type) {
    case GET_RATES_REQUEST:
      return {
        ...state,
        requestingGetRates: true,
        errorLoadingRates: false,
        errorMessage: null
      };
    case GET_RATES_SUCCESS:
      return {
        ...state,
        requestingGetRates: false,
        ratesData: action.payload
      };
    case GET_RATES_FAILURE:
      return {
        ...state,
        requestingGetRates: false,
        errorLoadingRates: true
      };
    case CLEAR_RATES_ERROR:
      return {
        ...state,
        errorLoadingRates: false
      };
    default:
      return state;
  }
}
